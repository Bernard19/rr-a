from db  import db

class RentalModel(db.Model):
    __tablename__ = 'rentals'

    id = db.Column(db.Integer, primary_key=True)
    cost = db.Column(db.Float(precision=2))
    address = db.Column(db.String(250))
    description = db.Column(db.String(450))

    owner_id = db.Column(db.String, db.ForeignKey('users.number'))
    owner = db.relationship('UserModel')

    def __init__(self, _id, cost, address, description):
        self.id = _id
        self.cost = cost
        self.address = address
        self.description = description

    def json(self):
        return {'id': self.id, 'cost': self.cost, 'address': self.address, 'description': self.description}

    @classmethod
    def find_by_id(cls, _id):
        return RentalModel.query.filter_by(id=_id).first()
    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()