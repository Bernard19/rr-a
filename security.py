from models.user import UserModel
from werkzeug.security import safe_str_cmp


def authenticate(number):
    user = UserModel.find_by_number(number, None)
    if user:
        return user

#ensure to correct identity method
def identity(payload):
    user_id = payload['identity']
    return UserModel.find_by_id(user_id, None)