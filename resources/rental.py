from flask_restful import Resource, reqparse
from flask_jwt import jwt_required
from models.rental import RentalModel

class Rental(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('id', type=int,
        required=True,
        help='')
    parser.add_argument('cost', type=float
        required=True,
        help='')
    parser.add_argument('address', type=str,
        required=True,
        help='')
    parser.add_argument('description',
        type=str,
        required=True,
        help=''
    )

    @jwt_required
    def get(self, _id):
        rental = RentalModel.find_by_id(_id)

        if rental:
            return rental.json()
        return {'message': 'Item is not found'}, 404

    def post(self, _id):
        if RentalModel.find_by_id(_id):
            return {'message': 'An item with name "{}" already exists'.format(_id)}, 404

        data = Rental.parser.parse_args()

        rental = RentalModel(NULL, data['cost'], data['address'], data['description'])

        try:
            rental.save_to_db()
        except:
            return {'message': 'An error occured inserting this item'}, 500

        return rental.json(), 201

    def put(self, _id):
        data = Item.parser.parse_args()
        
        rental = RentalModel.find_by_id(_id)

        if rental is None:
            rental = RentalModel(NULL, data['cost'], data['address'], data['description'])
        else:
            rental.cost = data['cost']
            rental.address = data['address']
            rental.description = data['description']

        rental.save_to_db()
        
        return rental.json()

    def delete(self, _id):
        rental = RentalModel.find_by_id(_id)
        if rental: #check this
            rental.delete_from_db()

        return {'message': 'Item deleted'}

class Rentals(Resource):
    def get(self):
        return {'rentals': [rental.json() for rental in RentalModel.query.all()]}